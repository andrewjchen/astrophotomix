#!/usr/bin/env python
'''
Astrophotomix-python
Andrew Chen
andrew.chen@berkeley.edu
'''


import pygame
import numpy
from pygame.locals import *

# the following line is not needed if pgu is installed
import sys; sys.path.insert(0, "..")

from pgu import gui
from gui7 import ColorDialog

W,H = 1200,600
W2,H2 = W/2,H/2

##You can initialize the screen yourself.
##::
screen = pygame.display.set_mode((W,H),SWSURFACE)
##

form = gui.Form()

image = pygame.image.load("moon.jpg").convert()
imagerect = image.get_rect()

inputsize = 10
slidersize = 20
sliderwidth = 200
sliderheight = 16

labelsize = 10

sidebarWidth = 500


class ImageTransf(gui.Table):
    def __init__(self,**params):
        gui.Table.__init__(self,**params)
        fg = (255,255,255)

        self.tr()
        self.td(gui.Label("astrophotomix-python",color=fg, width=30),colspan=2)

        self.tr()
        self.td(gui.Label("Select Celestial object: ",color=fg),align=1)
        w = gui.Select(value="moon", name="aobject")
        w.add("Moon","moon")
        w.add("Saturn", "saturn")
        w.add("Jupiter", "jupiter")
        w.add("Mars", "mars")
        self.td(w)

        '''
        self.build_attribute("Size (1000%)", "size", 1, 2500, 5000);
        self.build_attribute("alpha(byte)", "alpha", 1, 254, 255);
        '''
               
        self.build_attribute("Objective Focal Length (mm)", "ofocallength", 400, 5240, 8000);
        self.build_attribute("Objective Aperture (mm)", "oaperture", 40, 200, 800);
        self.build_attribute("Eyepiece Focal Length (mm)", "efocallength", 3, 55, 55);
        self.build_attribute("Apparent FOV", "afov", 43, 50, 84);
        
        #self.build_attribute("Object Size (Arcsec)", "aheight", 15, 1930, 10000);
        #Object Size
        self.tr()
        self.td(gui.Label("Object Size (Arcsec)", color=fg), align=0)
        self.tr()
        self.td(gui.Input("1930", size=inputsize, name="aheight-text"))
                
        self.build_label("Magnification", "magnification")
        self.build_label("Focal ratio", "focalratio")
        self.build_label("Exit Pupil", "exitpupil")
        self.build_label("Surface Brightness", "surfacebrightness")
        self.build_label("Linear height at prime focus(mm)", "lheight")

        '''
        self.tr()
        self.td(gui.Label("Full Screen: ",color=fg),align=1)
        self.td(gui.Switch(value=False,name='fullscreen')) 
        '''

    def build_attribute(self,label, name, x0, xc, xf):
        fg = (255,255,255)
        self.tr()
        self.td(gui.Label(label,color=fg),align=0)
        self.tr()
        self.td(gui.Input(str(xc),size=inputsize, name=(name + "-text")))
        e = gui.HSlider(xc,x0,xf,size=slidersize,width=sliderwidth,height=sliderheight,name=name)
        self.td(e)

    def build_label(self, label, name):
        fg = (255,255,255)
        self.tr()
        self.td(gui.Label(label, size=100,color=fg,align=0))
        self.td(gui.Input(label,width=200, name=name))



#basic attributes
prevsize, size = 2500, 250
prevalpha, alpha = 255, 255
cropsize = 255

# OPTICS--inputs
ofocallength = 2000
oaperture = 200
efocallength = 10
afov = 55
aheight = 1930

# OPTICS-- outputs
magnification = ofocallength / efocallength
focalratio = ofocallength / oaperture
exitpupil = efocallength / focalratio
surfacebrightness = 2 *  (exitpupil * exitpupil)
lheight = aheight * ofocallength / 57.3 #should be effective focal length

def updateFields():
    global size, alpha
    global ofocallength, oaperture, efocallength, afov, aheight
    global magnification, focalratio, exitpupil, surfacebrightness, lheight

    '''
    size= form['size'].value / 10
    form['size-text'].value = size

    alpha = form['alpha'].value
    form['alpha-text'].value = alpha
    '''
    
    ofocallength = form['ofocallength'].value
    form['ofocallength-text'].value = ofocallength

    oaperture = form['oaperture'].value
    form['oaperture-text'].value = oaperture

    efocallength = form['efocallength'].value
    form['efocallength-text'].value = efocallength

    afov = form['afov'].value
    form['afov-text'].value = afov

    #aheight = form['aheight'].value
    #form['aheight-text'].value = aheight

    aheight = int(form['aheight-text'].value)

    magnification = ofocallength / float(efocallength)
    focalratio = ofocallength / float(oaperture)
    exitpupil = efocallength / float(focalratio)
    surfacebrightness = 2 * (exitpupil * exitpupil)
    lheight = aheight * ofocallength / 57.3 #should be effective focal length


    form['magnification'].value = str(magnification) + "X"
    form['focalratio'].value = "f/" + str(focalratio)
    form['exitpupil'].value = str(exitpupil) + "mm"
    form['surfacebrightness'].value = str(surfacebrightness) + "%"
    form['lheight'].value = str(lheight) + "mm"


aobject_past = "moon"
def updateObject():
    global aobject_past
    global image, imagerect
    aobject_current = form['aobject'].value

    filename = ""
    if aobject_current is not aobject_past:
        if aobject_current is "moon":
            aheight = 1930
        if aobject_current is "saturn":
            aheight = 40
        if aobject_current is "jupiter":
            aheight = 30
        if aobject_current is "mars":
            aheight = 20
        form['aheight-text'].value = aheight
        filename = aobject_current + ".jpg"
        image = pygame.image.load(filename).convert()
        imagerect = image.get_rect()

    aobject_past = aobject_current

##Using App instead of Desktop removes the GUI background.  Note the call to app.init()
##::

# CLICK AND DRAG
centerxprev = (W-sidebarWidth)/2 + sidebarWidth
centeryprev = H2

centerx = (W-sidebarWidth)/2 + sidebarWidth
centery= H2

clickx = 0
clicky = 0

# OPTICS
objectiveFocalLength = 500
objectiveAperture = 50


app = gui.App()
t = ImageTransf()

c = gui.Container(align=-1,valign=-1)
c.add(t,0,0)

app.init(c)
##


def performVignette(alphas, size):
    xs, ys = alphas.shape
    for x in range (xs):
        for y in range(ys):
            x0 = x - xs/2 
            y0 = y - ys/2
            if( (x0 * x0) + (y0 * y0) < (size * size)):
                alphas[x,y] = 0
    return alphas;
        
def render():

    # MAGNIFICATION
    #imgsize =  int(magnification)
    #newrect = int(imagerect[2]*magnification/200),int(imagerect[3]*magnification/200)
    #newrect = imagerect[2], imagerect[3]
    newrect = int(magnification * aheight / 600 ), int(magnification * aheight/600)
    #print(newrect)
    image2 = pygame.transform.smoothscale(image, newrect)

    # SURFACE BRIGHTNESS
    image2.set_alpha(int(surfacebrightness * float(255)))

    #resizing
    posX = centerx - image2.get_width()/2
    posY = centery - image2.get_height()/2
    screen.blit(image2 , (posX, posY))

    vignette = pygame.Surface((W-sidebarWidth,H), 32).convert_alpha()
    vignette.fill((10, 10, 10))

    alphas = pygame.surfarray.pixels_alpha(vignette)
    alphas = performVignette(alphas, afov * 3)
    #print(alphas)
    del alphas
    vignette.unlock()
    screen.blit(vignette, (sidebarWidth, 0))
    screen.blit(pygame.Surface((sidebarWidth, H)), (0,0))


_form = form.results()
#reset()
_quit = 0
while not _quit:
    #if form['quantity'].value != _form['quantity']: adjust(form['quantity'].value-_form['quantity'])
    '''
    if form['fullscreen'].value != _form['fullscreen']:
        pygame.display.toggle_fullscreen()
    '''
    _form = form.results()

    #form['csize'].value = form['cropsize'].value
    
    screen.fill((0,0,0)) #clear the screen
    updateObject()
    updateFields()
    render() #renders the starfield


    for e in pygame.event.get():
        if e.type is QUIT: _quit = 1
        if e.type is KEYDOWN and e.key == K_ESCAPE: _quit = 1
        if e.type is MOUSEMOTION:
            #print "mouse at (%d, %d)" % e.pos
            if clickx is not 0 and clicky is not 0:
                centerx = centerxprev + e.pos[0] - clickx
                centery = centeryprev + e.pos[1] - clicky
            else:
                centerxprev = centerx
                centeryprev = centery

        if e.type is MOUSEBUTTONDOWN:
            #print "mouse down, at:" + str(e.pos[0]) + ", " + str(e.pos[1])
            if(e.pos[0] > sidebarWidth):
                clickx, clicky = e.pos

        if e.type is MOUSEBUTTONUP:
            #print "mouse up, at:" + str(e.pos[0]) + ", " + str(e.pos[1])
            clickx, clicky = (0,0)

        app.event(e)
    app.paint(screen)



    pygame.display.flip()
    pygame.time.wait(1)
##
