#!/usr/bin/env python
import pygame
import time
import sys
import math

def main():
    global window
    pygame.init()
    window = pygame.display.set_mode((640,480))
    
    image = pygame.image.load("lena.ppm").convert()
    imagerect = image.get_rect()

    x = 0
    deltaT = .01
    posX = 320
    posY = 240
    while 1:

        x = x+deltaT
        #print(x)
        
        #image.

        pygame.draw.rect(window, (1,1,1), (0, 0, 640, 640))

        window.fill((100,100,100))
        #window.fill(black)
        #posX = 200 * math.sin(x/1.0) + 320
        #posY = 200 * math.sin(x/0.5) + 240

        alpha = int((255/2.0) * math.sin(x) + (255/2.0))

        #image.set_alpha(alpha)
        #change_alpha(image, alpha)
        print(alpha)

        size = int(200 * math.sin(x) + 200)

        image2 = pygame.transform.smoothscale(image, (size, size))

        #resizing
        posX = 320 - image2.get_width()/2
        posY = 240 - image2.get_height()/2
        window.blit(image2 , (posX, posY))
        #window.blit(image , (posX + 5, posY + 5))
        pygame.display.flip()
        time.sleep(deltaT)


def change_alpha(surface,alpha=0.5):
    size = surface.get_size()
    try:
        for y in xrange(size[1]):
            for x in xrange(size[0]):
                r,g,b,a = surface.get_at((x,y))
                surface.set_at((x,y),(r,g,b,int(a*alpha)))
    except:
        return surface
    return surface


if __name__ == "__main__":
    main()
